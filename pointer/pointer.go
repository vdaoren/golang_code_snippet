package pointer

import (
	"fmt"
)

func RunPointer() {

	// var p *int // 未赋值的指针指向的是nil而不是0

	// // 直接输出指针所指向的地址
	// fmt.Println("*p:", p)

	var x = 100
	var p *int = &x
	// p地址的值
	fmt.Println("p:", p)
	// 取地址
	fmt.Println("&p:", &p)
	// 取值
	fmt.Println("*p:", *p)
	// 修改值
	*p = 19
	fmt.Println("*p:", *p)
}
