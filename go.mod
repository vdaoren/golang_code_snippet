module golangcodesnippet

go 1.12

replace (
	golangcodesnippet/ioopt => ./ioopt
	golangcodesnippet/pointer => ./pointer
)
