package log

import (
	"github.com/gogf/gf/g/os/gfile"
	"github.com/gogf/gf/g/os/glog"
)

var (
	e error
)

func init() {
	path := "./logs"
	glog.SetPath(path)
	if _, e = gfile.ScanDir(path, "*"); e != nil {
		panic("日志初始化失败:" + e.Error())
	}

}

// Info 基本消息日志
func Info(format string, v ...interface{}) {
	glog.Infofln(format, v...)
}

// Debug 调试日志消息
func Debug(format string, v ...interface{}) {
	glog.Debugfln(format, v...)
}

// Error 错误日志消息
func Error(format string, v ...interface{}) {
	glog.Errorfln(format, v...)
}
