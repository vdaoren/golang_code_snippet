package main

import (
	"context"
	"gin_grpc/common/log"
	"gin_grpc/config"
	pbMsg "gin_grpc/service/msg"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type server struct{} //服务对象
// SayHello 实现服务的接口 在proto中定义的所有服务都是接口
func (s *server) SayHello(ctx context.Context, in *pbMsg.HelloRequest) (*pbMsg.HelloReply, error) {
	log.Debug("消息获取到:%s", in.Name)
	return &pbMsg.HelloReply{Message: "Hello " + in.Name}, nil
}

func main() {
	lis, err := net.Listen("tcp", config.ServicePort())
	if err != nil {
		log.Error("failed to listen: %v", err)
	}
	s := grpc.NewServer() //起一个服务
	pbMsg.RegisterGreeterServer(s, &server{})
	// 注册反射服务 这个服务是CLI使用的 跟服务本身没有关系
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Error("failed to serve: %v", err)
	}

}
