module gin_grpc

go 1.13

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/gogf/gf v1.8.3
	github.com/golang/protobuf v1.3.1
	google.golang.org/appengine v1.6.2 // indirect
	google.golang.org/grpc v1.23.0
)
