package route

import (
	"gin_grpc/config"

	"github.com/gin-gonic/gin"

	v1User "gin_grpc/gateway/controller/v1/user"
)

// InitRouter 路由初始化
func InitRouter() {
	router := gin.Default()
	v1Api := router.Group("api/v1")
	{
		v1Api.GET("user/reg", v1User.Reg)
	}
	router.Run(config.GatewayPort())
}
