package main

import (
	"gin_grpc/gateway/route"
)

func main() {
	route.InitRouter()
}
