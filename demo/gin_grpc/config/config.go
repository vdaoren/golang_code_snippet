package config

import (
	"gin_grpc/common/log"
	"io/ioutil"

	"github.com/gogf/gf/g/encoding/gjson"
)

var (
	e        error
	fileByte []byte
	jsonObj  *gjson.Json
)

func init() {
	fileByte, e = ioutil.ReadFile("./config/config.json")
	if e != nil {
		log.Error("配置文件打开出错:%s", e.Error())
	} else if jsonObj, e = gjson.DecodeToJson(fileByte); e != nil {
		log.Error("%s", e.Error())
	}
}

// GatewayPort 网关端口号
func GatewayPort() string {
	return jsonObj.GetString("gateway_port")
}

// ServicePort 服务端口号
func ServicePort() string {
	return jsonObj.GetString("service_port")
}
