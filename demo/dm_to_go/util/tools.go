package util

import (
	"fmt"
	"io/ioutil"
	"os/exec"
)

// Register 注册dll
func Register(dllPath string) {
	cmdObj := exec.Command("cmd", "dir")

	iod, e := cmdObj.StdoutPipe()
	if e != nil {
		fmt.Println("执行命令出错2:", e.Error())
		return
	}
	defer iod.Close()

	if e := cmdObj.Start(); e != nil {
		fmt.Println("执行命令出错1:", e.Error())
		return
	}

	sByte, e := ioutil.ReadAll(iod)
	if e != nil {
		fmt.Println("执行命令出错2:", e.Error())
		return
	}
	fmt.Println("执行命令返回:", string(sByte))

}

// UnRegister 反注册dll
func UnRegister(dllPath string) {

}
