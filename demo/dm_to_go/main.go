package main

import (
	"fmt"

	ole "github.com/go-ole/go-ole"
	"github.com/go-ole/go-ole/oleutil"
)

func main() {
	fmt.Println("dm to go")

	// 在注册表下查找相应id
	// v, e := ole.CLSIDFromProgID("dm.dmsoft")
	// fmt.Println("查找的id是:", v)
	// // v, e := ole.CLSIDFromString("dm.dmsoft")
	// v1, e := ole.IIDFromString(v.String())
	// if e != nil {
	// 	panic(e.Error())
	// }
	// fmt.Println("查找的id是:", v1)

	// // 根据接口类型从programID创建对象。
	// // 程序ID可以是程序ID，也可以是应用程序字符串。
	// unknown, err := oleutil.CreateObject("dm.dmsoft")
	// if err != nil {
	// 	panic(err.Error())
	// }

	// unknown1, err := oleutil.ClassIDFrom("dm.dmsoft")
	// if err != nil {
	// 	panic(err.Error())
	// }
	// fmt.Println(unknown1)

	// 1.初始化
	ole.CoInitialize(0)
	v, e := ole.CLSIDFromProgID("dm.dmsoft")
	if e != nil {
		fmt.Println(e.Error())
	}
	fmt.Println(v)
	fmt.Println(ole.IID_IDispatch)
	// 2.根据接口类型从程序id创建对象。
	unknown, err := oleutil.CreateObject("dm.dmsoft")
	if err != nil {
		panic(err.Error())
	}
	// 3.更具id查找接口
	// ole.IID_IDispatch 固定传入
	dmObj, e := unknown.QueryInterface(ole.IID_IDispatch)
	if e != nil {
		fmt.Println("dmObj错误:", e.Error())
	}
	fmt.Println(dmObj)
	// 调用方法
	vv, e := dmObj.CallMethod("Ver")
	if e != nil {
		fmt.Println("vv错误:", e.Error())
	}
	fmt.Println("插件版本号是:", vv.ToString())
}
