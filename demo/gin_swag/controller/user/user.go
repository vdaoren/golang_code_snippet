package user

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// InfoRep 用户信息
type InfoRep struct {
	UserName string `json:"user_name"`
	PassWord string `json:"password"`
}

// InfoReq 用户信息
type InfoReq struct {
	Code int `json:"code"`
}

// Register 注册
// @Tags 用户模块
// @Summary 用户注册
// @Description 这是简介
// @Accept  json
// @Produce  json
// @Param body body user.InfoRep true "用户信息"
// @Success 200 {object} user.InfoReq "Code=1000 Body="返回参数详情 Code!=1000,Body=错误详情"
// @Router /api/v1/user/register [post]
func Register(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"code": 6,
	})
}
