package v1

import (
	"gin_swag/controller/user"

	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"

	// swag api
	_ "gin_swag/docs"

	"github.com/gin-gonic/gin"
)

// InitRouter v1路由初始
func InitRouter() {
	gin.SetMode(gin.DebugMode)
	router := gin.Default()
	router.GET("/docs/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	api := router.Group("api/v1")
	{
		api.POST("user/register", user.Register)
	}
	router.Run()
}
