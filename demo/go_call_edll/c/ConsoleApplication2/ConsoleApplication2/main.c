﻿#include <stdio.h>
#include <windows.h>

typedef char(*ret_str)(char str[]);

int main() {
	HINSTANCE dllA; 
	ret_str f1;

	dllA = LoadLibrary("C:\\Users\\shuhu\\MyWork\\Dev\\Code\\Golang\\go_call_edll\\c\\ConsoleApplication2\\Debug\\etest.dll"); 
	if (NULL == dllA) { 
		printf("无法加载dll！\n"); 
	} 
	f1 = (ret_str)GetProcAddress(dllA, "ret_str");
	if (NULL == f1) { 
		printf("找不到函数地址！\n"); 
	}
	char str[] = "love";
	printf("调用结果：%s\n", f1(str));
	return 0;
}