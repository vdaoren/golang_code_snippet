package ioopt

import (
	"encoding/binary"
	"fmt"
	"io/ioutil"
	"os"
)

// ReadBmpFileHead 获取bmp文件头
func ReadBmpFileHead(bmpPath string) {
	file, _ := os.Open(bmpPath)
	defer file.Close()

	var headA, headB byte

	/*
		binary.Read(文件句柄/文件指针,字节序,接口)
		字节序:
			windows/Linux:小端
			MacOs:大端
	*/
	binary.Read(file, binary.LittleEndian, &headA)
	binary.Read(file, binary.LittleEndian, &headB)
	fmt.Printf("%c%c", headA, headB)
}

// ReadBmpFile 读取bmp文件
func ReadBmpFile(bmpPath string) {
	file, _ := os.Open(bmpPath)
	defer file.Close()

	// 头
	var headA, headB byte
	binary.Read(file, binary.LittleEndian, &headA)
	binary.Read(file, binary.LittleEndian, &headB)
	fmt.Printf("文件头:%c%c\n", headA, headB)
}

// DelBmoFielSign 删除bmp文件表示头
func DelBmoFielSign(binaryFilePath string) {
	file, _ := ioutil.ReadFile(binaryFilePath)
	fmt.Printf("%X %X \n", file[0], file[1])
	// funck
	var s1, s2 uint8
	s1 = '7'
	s2 = 'z'
	file[0] = s1
	file[1] = s2
	fmt.Printf("%X %X \n", file[0], file[1])
	newFielByte := file
	ioutil.WriteFile(binaryFilePath, newFielByte, 0664)
	fmt.Println()
}
