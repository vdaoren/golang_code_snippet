package ioopt

import (
	"fmt"
	"io/ioutil"
)

// IOutil io.Reader
func IOutilRead(file_path string) {
	file, _ := ioutil.ReadFile(file_path)
	for i := 0; i < len(file); i++ {
		fmt.Printf("%X ", file[i])
	}
	fmt.Println()
}

// IOutilWrite 写入
func IOutilWrite(file_path string) {
	byteData := []byte("abc")
	for i := 0; i < len(byteData); i++ {
		fmt.Printf("%X ", byteData[i])
	}
	// 直接清空覆盖
	ioutil.WriteFile(file_path, byteData, 0664)
	fmt.Println()
}

// BinaryFileOpt 二进制文件操作
func BinaryFileOpt(binaryFilePath string) {
	file, _ := ioutil.ReadFile(binaryFilePath)
	fmt.Printf("%X %X \n", file[0], file[1])
	file[0] = 0
	file[1] = 0
	fmt.Printf("%X %X \n", file[0], file[1])
	newFielByte := file
	ioutil.WriteFile(binaryFilePath, newFielByte, 0664)
	fmt.Println()
}
