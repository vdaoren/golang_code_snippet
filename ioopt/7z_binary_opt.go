package ioopt

import (
	"encoding/binary"
	"fmt"
	"io/ioutil"
	"os"
)

// Reaf7ZFileHead 读取7z文件头
func Reaf7ZFileHead(zFilePath string) {
	file, _ := os.Open(zFilePath)
	defer file.Close()
	var headA, headB byte
	binary.Read(file, binary.LittleEndian, &headA)
	binary.Read(file, binary.LittleEndian, &headB)
	fmt.Printf("%c%c", headA, headB)
}

// Read7zFile 获取7z文件头
func Read7zFile(zFilePath string) {
	file, _ := ioutil.ReadFile(zFilePath)
	// 先是固定的6个字节的值, 前两个字节的值是字母 '7' 和'z' 的ascii值.  后面四个字节是固定的: 0xbc, 0xaf, 0x27, 0x1c
	fmt.Printf("%X %X \n", file[0], file[1])
	fmt.Printf("%X \n", file[5])
	fmt.Printf("版本号:%X.%X \n", file[6], file[7])
	for i := 0; i < len(file); i++ {
		if i >= 7 {
			file[i] = 0
		}
	}
	newFielByte := file
	ioutil.WriteFile(zFilePath, newFielByte, 0664)
	fmt.Println()
}
