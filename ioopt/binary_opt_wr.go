/*
	二进制文件头更改
*/

package ioopt

import (
	"encoding/binary"
	"fmt"
	"os"
)

// bmp文件头信息
type BitmapInfoHeader struct {
	Size           uint32
	Width          int32
	Height         int32
	Places         uint16
	BitCount       uint16
	Compression    uint32
	SizeImage      uint32
	XperIsPerMeter int32
	YperIsPerMeter int32
	ClsrUsed       uint32
	ClrImgportant  uint32
}

// ReadBinaryFile 读取二进制文件
func ReadBinaryFile(bmpPath string) {
	file, _ := os.Open(bmpPath)
	defer file.Close()

	// 头
	var headA, headB byte
	binary.Read(file, binary.LittleEndian, &headA)
	binary.Read(file, binary.LittleEndian, &headB)
	fmt.Printf("文件头:%c%c\n", headA, headB)

	// 大小
	var size uint32
	binary.Read(file, binary.LittleEndian, &size)
	fmt.Println("文件大小:", size)

	// 保留字节
	var reservedA, reservedB uint16
	binary.Read(file, binary.LittleEndian, &reservedA)
	binary.Read(file, binary.LittleEndian, &reservedB)
	fmt.Println("文件保留字节:", reservedA, reservedB)

	// 文件偏移，文件内从哪里开始
	var offbits uint32
	binary.Read(file, binary.LittleEndian, &offbits)
	fmt.Println("文件文件偏移:", offbits)

	bitmapInfoHeader := new(BitmapInfoHeader)
	binary.Read(file, binary.LittleEndian, bitmapInfoHeader)
	fmt.Println("图片头信息:", bitmapInfoHeader)
	fmt.Println("图片宽度:", bitmapInfoHeader.Width)
	fmt.Println("图片高度:", bitmapInfoHeader.Height)
}

// WriteBinaryFile 写入
func WriteBinaryFile(bmpPath string) {
	file, _ := os.Open(bmpPath)
	defer file.Close()

}
