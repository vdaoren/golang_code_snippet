# io文件操作

## 关于golang的IO包

### 1.1 IO 操作原语定义与基础接口

在源代码中，对于 IO 流，定义了四个基本操作原语，分别用 Reader，Writer，Closer，Seeker 接口表达二进制流读、写、关闭、寻址操作.
```Go

// 读
type Reader interface {
	Read(p []byte) (n int, err error)
}

// 写
type Writer interface {
	Write(p []byte) (n int, err error)
}

// 关闭
type Closer interface {
	Close() error
}

// 寻址
type Seeker interface {
	Seek(offset int64, whence int) (int64, error)
}

```

然后，定义了一些原语组合，表达一些常用的流文件的处理能力。如只读、只写、可读写流。
```Go
// ReadWriter is the interface that groups the basic Read and Write methods.
type ReadWriter interface {
	Reader
	Writer
}

type ReadCloser interface {...}
type WriteCloser interface {...}
type ReadWriteCloser interface {...}

type ReadSeeker interface {...}
type WriteSeeker interface {...}
type ReadWriteSeeker interface {...}

```

为了兼容以往流编程习惯，IO 库定义了一些常见的基本操作，例如，读写一个字符，seek 到特定位置读写，字串的读写等
```Go
type ReaderAt interface {
	ReadAt(p []byte, off int64) (n int, err error)
}

type WriterAt interface {...}

type ByteReader interface {
	ReadByte() (byte, error)
}

type ByteWriter interface {...}

type RuneReader interface {
	ReadRune() (r rune, size int, err error)
}

// stringWriter is the interface that wraps the WriteString method.
type stringWriter interface {
	WriteString(s string) (n int, err error)
}

```